{ nixpkgs ? import <nixpkgs> {} }:

let

  inherit (nixpkgs) pkgs;

  ghc = pkgs.haskellPackages.ghcWithPackages (ps: with ps; [
    pandoc
  ]);

in

  pkgs.stdenv.mkDerivation {
    name = "latexfilter";
    src = ./.;
    buildInputs = [ ghc ];
    installPhase = ''
      mkdir -p $out/bin
      ghc -Wall -O2 latexfilter.hs
      cp latexfilter $out/bin/
    '';
    #shellHook = "eval $(egrep ^export ${ghc}/bin/ghc)";
  }

