{-# LANGUAGE OverloadedStrings #-}

import Text.Pandoc
import Text.Pandoc.JSON

inFramed :: Block -> Block
inFramed x = Div nullAttr 
    [ RawBlock (Format "latex") "~\\\\ \\begin{mdframed}[backgroundcolor=yellow!20] "
    , x
    , RawBlock (Format "latex") " \\end{mdframed}~\\\\"
    ]

inFramed2 :: Block -> Block
inFramed2 x = Div nullAttr 
    [ RawBlock (Format "latex") "~\\\\ \\begin{mdframed}[backgroundcolor=red!20]~"
    , x
    , RawBlock (Format "latex") " \\end{mdframed}~\\\\"
    ]

main :: IO ()
main = toJSONFilter behead
  where behead x@(CodeBlock _ _) = inFramed x
        behead x@(BlockQuote _) = inFramed2 x
        behead x = x

