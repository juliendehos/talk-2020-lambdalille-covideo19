{-# LANGUAGE OverloadedStrings #-}

import           Control.Concurrent (threadDelay)
import           Control.Monad (forever)
import qualified Data.ByteString.Lazy as BS
import qualified Network.WebSockets as WS
import           System.Environment (getArgs)

main :: IO ()
main = do
    args <- getArgs
    img <- BS.readFile "static/gary.jpg"
    case args of
        [ip, portStr] -> do
            putStrLn $ "connecting " <> ip <> " on port " <> portStr
            let app = clientApp img
                port = read portStr
            WS.runClient ip port "" app
        _ -> putStrLn "usage: <ip> <port>"

clientApp :: BS.ByteString -> WS.ClientApp ()
clientApp img conn = forever $ do
    WS.sendBinaryData conn img
    threadDelay 500000

