# talk-2020-lambdalille-covideo19

2020-04-09

Coder un streamer video en 135 lignes de Haskell et en 1 week-end

Pas de concept avancé, juste un petit exemple d'utilisation pratique de Haskell : serveur web, websockets, accès concurrents, capture d'écran. En bonus : construire une image Docker avec Nix et la déployer sur Heroku.

- [slides](https://juliendehos.gitlab.io/talk-2020-lambdalille-covideo19/index.pdf)

- [dépôt](https://gitlab.com/juliendehos/talk-2020-lambdalille-covideo19)

- [covideo19](https://gitlab.com/juliendehos/covideo19)

- [meetup](https://www.meetup.com/fr-FR/LambdaLille/events/269852942/)

- [video](files/talk-2020-lambdalille-covideo19.mp4)

- [video du meetup](https://www.youtube.com/watch?v=6CO98XBsNiY)

