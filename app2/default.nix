let
  pkgs = import <nixpkgs> {};
  drv = pkgs.haskellPackages.callCabal2nix "covideo19" ./. {};

in if pkgs.lib.inNixShell then drv.env else drv
