{-# LANGUAGE OverloadedStrings #-}

import           Control.Concurrent (threadDelay)
import           Control.Monad (forever)
import qualified GI.Gdk as Gdk
import qualified GI.Gtk as Gtk
import           GI.GdkPixbuf.Objects.Pixbuf
import           GI.GObject.Objects.Object
import qualified Network.WebSockets as WS
import           System.Environment (getArgs)

initGtk :: IO Gdk.Window
initGtk = do
    _ <- Gtk.init Nothing
    Just screen <- Gdk.screenGetDefault
    Gdk.screenGetRootWindow screen

main :: IO ()
main = do
    window <- initGtk
    args <- getArgs
    case args of
        [ip, portStr] -> do
            putStrLn $ "connecting " <> ip <> " on port " <> portStr
            let app = clientApp window 
                port = read portStr
            WS.runClient ip port "" app
        _ -> putStrLn "usage: <ip> <port>"

clientApp :: Gdk.Window -> WS.ClientApp ()
clientApp window conn = forever $ do
    Just pxbuf <- Gdk.pixbufGetFromWindow window 0 0 800 600
    WS.sendBinaryData conn =<< pixbufSaveToBufferv pxbuf "jpeg" ["quality"] ["50"]
    objectUnref pxbuf
    threadDelay 500000

