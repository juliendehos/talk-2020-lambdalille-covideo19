all: index.html index.pdf

index.html: index.md
	pandoc index.md -o index.html -s -i -t revealjs -V revealjs-url=files/local/revealjs -V theme=white -V transition=fade -V slideNumber=true -V fragments=false -V center=true -V history=false --css files/my-revealjs.css --css files/local/syntax/pygments.css --katex=files/local/katex/ --section-divs --slide-level=2
	mkdir -p files/local
	find archives -name "*.tar.gz" -exec tar zxf {} -C files/local \;

index.pdf: index.md # custom/latexfilter
	pandoc index.md -t beamer -o index.pdf -H custom/my-beamer-header.sty --section-divs --slide-level=2 --filter latexfilter
	# pandoc index.md -t beamer -o index.tex -H custom/my-beamer-header.sty --section-divs --slide-level=2

# custom/latexfilter: custom/latexfilter.hs
# 	nix-shell -p "haskellPackages.ghcWithPackages (pkgs: [pkgs.pandoc])" --run "ghc -O2 -Wall custom/latexfilter.hs"

clean:
	rm -f index.html index.pdf custom/latexfilter.hi custom/latexfilter custom/latexfilter.o

