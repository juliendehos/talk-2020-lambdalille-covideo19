with import <nixpkgs> {};

stdenv.mkDerivation rec {
  name = "talk-2020-lambdalille-covideo19";
  src = ./.;
  buildInputs = [
    (callPackage ./custom/latexfilter.nix {})
    gnumake
    pandoc
    (texlive.combine {
      inherit (texlive) scheme-small mdframed needspace;
    })
  ];
  installPhase = ''
    mkdir -p $out
    cp -R files index.html index.pdf $out
  '';
}

