{-# LANGUAGE OverloadedStrings #-}

import qualified Data.ByteString.Lazy as BS
import           Data.IORef (readIORef, IORef, newIORef)
import           Data.Maybe (fromMaybe)
import           System.Environment (lookupEnv)
import qualified Web.Scotty as SC

httpApp :: IORef BS.ByteString -> SC.ScottyM ()
httpApp imgRef = do
    SC.get "/" $ SC.file "static/index.html"
    SC.get "/img" $ do
        SC.addHeader "Content-Type" "image/jpeg"
        SC.raw =<< SC.liftAndCatchIO (readIORef imgRef)

main :: IO ()
main = do
    img <- BS.readFile "static/bob.jpg"
    imgRef <- newIORef img
    port <- read . fromMaybe "3000" <$> lookupEnv "PORT"
    putStrLn $ "listening port " ++ show port ++ "..."
    SC.scotty port $ do
        httpApp imgRef

