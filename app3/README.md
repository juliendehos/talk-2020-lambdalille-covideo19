# covideo19

- run server:

```
nix-shell --run "cabal run covideo-serve"
```

- run recorder:

```
nix-shell --run "cabal run covideo-record 127.0.0.1 3000"
```

- build a docker image:

```
nix-build nix/docker.nix
docker load < result
```

- deploy server on heroku:

```
heroku login
heroku container:login
heroku create covideo19test
docker tag covideo19:test registry.heroku.com/covideo19test/web
docker push registry.heroku.com/covideo19test/web
heroku container:release web --app covideo19test
```

