let
  pkgs = import <nixpkgs> {};
  app-src = ../. ;
  app = pkgs.haskellPackages.callCabal2nix "covideo19" ../. {};
in
  pkgs.runCommand "covideo19" { inherit app; } ''
    mkdir -p $out/{bin,static}
    cp ${app}/bin/covideo19-serve $out/bin/
    cp ${app-src}/static/* $out/static/
  ''

