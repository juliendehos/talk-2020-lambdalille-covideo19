{ pkgs ? import <nixpkgs> {} }:
let
  app = import ./release.nix;
  entrypoint = pkgs.writeScript "entrypoint.sh" ''
    #!${pkgs.stdenv.shell}
    $@
  '';
in
  pkgs.dockerTools.buildLayeredImage {
    name = "covideo19";
    tag = "test";
    config = {
      WorkingDir = "${app}";
      Entrypoint = [ entrypoint ];
      Cmd = [ "${app}/bin/covideo19-serve" ];
    };
  }

