{-# LANGUAGE OverloadedStrings #-}

import           Control.Concurrent (threadDelay)
import           Control.Monad (forever, when)
import qualified GI.Gdk as Gdk
import qualified GI.Gtk as Gtk
import           GI.GdkPixbuf.Enums
import           GI.GdkPixbuf.Objects.Pixbuf
import           GI.GObject.Objects.Object
import qualified Network.WebSockets as WS
import           System.Environment (getArgs)

createCursor :: IO Pixbuf
createCursor = do
    Just cursorPix0 <- pixbufNew ColorspaceRgb False 8 12 12
    Just cursorPix1 <- pixbufNew ColorspaceRgb False 8 8 8
    pixbufFill cursorPix0 0x000000ff
    pixbufFill cursorPix1 0xffff00ff
    pixbufCopyArea cursorPix1 0 0 8 8 cursorPix0 2 2
    return cursorPix0

initGtk :: IO (Gdk.Device, Gdk.Window)
initGtk = do
    _ <- Gtk.init Nothing
    Just screen <- Gdk.screenGetDefault
    display <- Gdk.screenGetDisplay screen
    seat <- Gdk.displayGetDefaultSeat display
    Just device <- Gdk.seatGetPointer seat
    window <- Gdk.screenGetRootWindow screen
    return (device, window)

main :: IO ()
main = do
    args <- getArgs
    cursorPix <- createCursor
    (device, window) <- initGtk
    case args of
        [ip, portStr] -> do
            putStrLn $ "connecting " <> ip <> " on port " <> portStr
            let app = clientApp device cursorPix window 
                port = read portStr
            WS.runClient ip port "" app
        _ -> putStrLn "usage: <ip> <port>"

clientApp :: Gdk.Device -> Pixbuf -> Gdk.Window -> WS.ClientApp ()
clientApp device cursorPix window conn = forever $ do
    Just pxbuf <- Gdk.pixbufGetFromWindow window 0 0 800 600
    (_, xi, yi) <- Gdk.deviceGetPosition device
    when (xi > 0 && xi < 800 - 12 && yi > 0 && yi < 600 - 12)
        $ pixbufCopyArea cursorPix 0 0 12 12 pxbuf xi yi
    WS.sendBinaryData conn =<< pixbufSaveToBufferv pxbuf "jpeg" ["quality"] ["50"]
    objectUnref pxbuf
    threadDelay 500000

