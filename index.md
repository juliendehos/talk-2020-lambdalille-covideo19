---
title: Coder un streamer video en 135 lignes de Haskell et en 1 week-end
date: Lambda Lille 2020 (remote)
author: Julien Dehos
---


## Contexte

- je suis enseignant-chercheur à l'Université du Littoral

- Covid-19 $\Rightarrow$ enseignement à distance

- Département Informatique : ouverture d'un serveur Discord (texte + audio + partage d'écran)

- ce que j'avais déjà : support de cours sur le web, dépôts Gitlab


## Problème

- ce que j'avais pas : partage d'une zone de l'écran pour les TP (j'ai qu'un écran et je switche tout le temps entre Terminal, Vim, Firefox et Mypaint)

- connection ADSL $\Rightarrow$ upload inférieur à 1 Mbits/s 

- au moins 20 "spectateurs"

- testés mais rejetés : Discord, Zoom, Obs+Twitch, Gstreamer
  (écran complet ou 1 fenêtre, latence, débit...)

## Solution envisagée

- un serveur HTTP qui fournit l'image courante à des clients web

- un programme sur ma machine qui envoie une capture d'écran au server via un websocket

- une image jpeg basse qualité à 2 fps suffit

![](files/archi.png){width="80%"}

## Disclaimer

> 
> C'est du code moche, fait dans l'urgence !
> 

- on est averti le vendredi

- j'ai TP le mardi suivant

- et il faut aussi faire une VM + une page web pour les consignes


## Jalon 1 : serveur web

- une route `/` pour la page `index.html`

- une route `/img` pour l'image jpeg

- bibliothèque Haskell `scotty`

-------------------------------------------------------------------------------

- `codevideo19-serve.hs` :

```haskell
main :: IO ()
main = do
    img <- BS.readFile "static/bob.jpg"
    imgRef <- newIORef img
    port <- read . fromMaybe "3000" <$> lookupEnv "PORT"
    putStrLn $ "listening port " ++ show port ++ "..."
    SC.scotty port $ do
        httpApp imgRef

httpApp :: IORef BS.ByteString -> SC.ScottyM ()
httpApp imgRef = do
    SC.get "/" $ SC.file "static/index.html"
    SC.get "/img" $ do
        SC.addHeader "Content-Type" "image/jpeg"
        SC.raw =<< SC.liftAndCatchIO (readIORef imgRef)
```

-------------------------------------------------------------------------------

- `static/index.html` :

```html
<html>
    <head>
        <meta charset="utf-8"/>
    </head>
    <body>
        <img id="my_img"> </img>
        <script>
            function updateImg() {
                fetch("img")
                    .then(response => response.blob())
                    .then(function(myBlob){
                        URL.revokeObjectURL(my_img.src);
                        my_img.src = URL.createObjectURL(myBlob);
                    });
            }
            const my_interval = setInterval(updateImg, 500);
        </script>
    </body>
</html>
```

-------------------------------------------------------------------------------

![](files/app0.png){width="60%"}



## Jalon 2 : websockets 

- le client streamer envoie l'image via un websocket

- le server recupère l'image sur le websocket et la rend disponible en HTTP

- bibliothèque Haskell `websockets`

-------------------------------------------------------------------------------

- `covideo19-record.hs` :

```haskell
main :: IO ()
main = do
    img <- BS.readFile "static/gary.jpg"
    args <- getArgs
    case args of
        [ip, portStr] -> do
            putStrLn $ "connecting " <> ip <> " on port " <> portStr
            let app = clientApp img
                port = read portStr
            WS.runClient ip port "" app
        _ -> putStrLn "usage: <ip> <port>"

clientApp :: BS.ByteString -> WS.ClientApp ()
clientApp img conn = forever $ do
    WS.sendBinaryData conn img
    threadDelay 500000
```

-------------------------------------------------------------------------------

- `covideo19-serve.hs` :

```haskell
main :: IO ()
main = do
    -- ...
    SC.scotty port $ do
        SC.middleware (wsApp imgRef)
        httpApp imgRef

wsApp :: IORef BS.ByteString -> Application -> Application
wsApp imgRef =
    websocketsOr WS.defaultConnectionOptions (wsHandle imgRef)

wsHandle :: IORef BS.ByteString -> WS.PendingConnection -> IO ()
wsHandle imgRef pc = do
    conn <- WS.acceptRequest pc
    forever (WS.receiveData conn >>= atomicWriteIORef imgRef)

httpApp :: IORef BS.ByteString -> SC.ScottyM ()
-- ...
```


-------------------------------------------------------------------------------

![](files/app1.png){width="60%"}


## Jalon 3 : capture d'écran

- l'image envoyée par le client streamer est une capture de l'écran

- bibliothèque Haskell `haskell-gi` (et ses dérivées)

-------------------------------------------------------------------------------

- `covideo19-record.hs` :

```haskell
main :: IO ()
main = do
    window <- initGtk
    -- ...

initGtk :: IO Gdk.Window
initGtk = do
    _ <- Gtk.init Nothing
    Just screen <- Gdk.screenGetDefault
    Gdk.screenGetRootWindow screen

clientApp :: Gdk.Window -> WS.ClientApp ()
clientApp window conn = forever $ do
    Just pxbuf <- Gdk.pixbufGetFromWindow window 0 0 800 600
    img <- pixbufSaveToBufferv pxbuf "jpeg" ["quality"] ["50"]
    WS.sendBinaryData conn img
    objectUnref pxbuf
    threadDelay 500000
```

-------------------------------------------------------------------------------

![](files/app2.png){width="60%"}


## Image Docker

- `release.nix` :

```nix
let
  pkgs = import <nixpkgs> {};
  app-src = ./. ;
  app = pkgs.haskellPackages.callCabal2nix "covideo19" ./. {};
in
  pkgs.runCommand "covideo19" { inherit app; } ''
    mkdir -p $out/{bin,static}
    cp ${app}/bin/covideo19-serve $out/bin/
    cp ${app-src}/static/* $out/static/
  ''
```

-------------------------------------------------------------------------------

- `docker.nix` :

```nix
{ pkgs ? import <nixpkgs> {} }:
let
  app = import ./release.nix;
  entrypoint = pkgs.writeScript "entrypoint.sh" ''
    #!${pkgs.stdenv.shell}
    $@
  '';
in
  pkgs.dockerTools.buildLayeredImage {
    name = "covideo19";
    tag = "test";
    config = {
      WorkingDir = "${app}";
      Entrypoint = [ entrypoint ];
      Cmd = [ "${app}/bin/covideo19-serve" ];
    };
  }
```



## Déploiement

- construire l'image Docker :

```bash
nix-build docker.nix
docker load < result
```

- déployer l'image sur Heroku :

```bash
heroku login
heroku container:login
heroku create covideo19test
docker tag covideo19:test registry.heroku.com/covideo19test/web
docker push registry.heroku.com/covideo19test/web
heroku container:release web --app covideo19test
```

- lancer le stream :

```bash
covideo19-record covideo19.herokuapp.com 80 
```


## Résultat

- ici, 60 lignes de Haskell

- mais dans la vraie appli :

    - affichage du curseur
    - paramètres supplémentaires (région à streamer, compression)
    - clé d'autorisation pour streamer
    - page de monitoring
    - gestion d'erreur (un peu)


## Retour d'expérience

- conclusion 1 : 

    - l'appli est moche mais bien pratique pour mon cas d'utilisation

    - testée en 800x600, 2 fps, qualité 25% (jusqu'à 25 clients web)

- conclusion 2 : 

    - Haskell c'est cool : typage, fonctionnel pur, libs...

    - je connaissais déjà scotty/websockets/docker/heroku $\Rightarrow$ RAS

    - je connaissais presque pas haskell-gi $\Rightarrow$ j'ai un peu galéré pour
      trouver les bonnes docs et exemples


## Références

- projet : <https://gitlab.com/juliendehos/covideo19>

- slides : <https://gitlab.com/juliendehos/talk-2020-lambdalille-covideo19>


## Merci ! Questions ou commentaires ?


